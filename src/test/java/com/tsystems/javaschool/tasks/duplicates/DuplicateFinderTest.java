package com.tsystems.javaschool.tasks.duplicates;

import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.util.List;
import java.util.TreeMap;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class DuplicateFinderTest {

    private DuplicateFinder duplicateFinder = new DuplicateFinder();

    @Test(expected = IllegalArgumentException.class)
    public void test() {
        //run
        duplicateFinder.process(null, new File("a.txt"));

        //assert : exception
    }

    @Test(expected = IllegalArgumentException.class)
    public void test1() {
        //run
        duplicateFinder.process(new File("a.txt"), null);

        //assert : exception
    }

    @Test
    public void test2() {
        //given
        List<String> input = Stream.of("123", "", "", "123", "A", "ABC", "TRE", "AB", "A").collect(Collectors.toList());
        TreeMap<String, Integer> expectedResult = new TreeMap<>();
        expectedResult.put("123", 2);
        expectedResult.put("", 2);
        expectedResult.put("A", 2);
        expectedResult.put("ABC", 1);
        expectedResult.put("TRE", 1);
        expectedResult.put("AB", 1);
        //run
        TreeMap<String, Integer> resultTree = duplicateFinder.findDuplicates(input);
        //assert
        boolean result;
        result = resultTree.entrySet().containsAll(expectedResult.entrySet())
                && resultTree.size() == expectedResult.size();
        Assert.assertTrue(result);
    }

    @Test
    public void test3() {
        //given
        List<String> input = Stream.of("abc", "given", "aaabbbccc", "une", "abcd", "abc", "given","aaabbbcc").collect(Collectors.toList());
        TreeMap<String, Integer> expectedResult = new TreeMap<>();
        expectedResult.put("abc", 2);
        expectedResult.put("given", 2);
        expectedResult.put("aaabbbccc", 1);
        expectedResult.put("aaabbbcc", 1);
        expectedResult.put("une", 1);
        expectedResult.put("abcd", 1);
        //run
        TreeMap<String, Integer> resultTree = duplicateFinder.findDuplicates(input);
        //assert
        boolean result;
        result = resultTree.entrySet().containsAll(expectedResult.entrySet())
                && resultTree.size() == expectedResult.size();
        Assert.assertTrue(result);
    }

    @Test
    public void test4() {
        //given
        List<String> input = Stream.of("Find", "Something", "On this scope", "On", "this", "something", "Find").collect(Collectors.toList());
        TreeMap<String, Integer> expectedResult = new TreeMap<>();
        expectedResult.put("Find", 2);
        expectedResult.put("Something", 1);
        expectedResult.put("On this scope", 1);
        expectedResult.put("On", 1);
        expectedResult.put("this", 1);
        expectedResult.put("something", 1);
        //run
        TreeMap<String, Integer> resultTree = duplicateFinder.findDuplicates(input);
        //assert
        boolean result;
        result = resultTree.entrySet().containsAll(expectedResult.entrySet())
                && resultTree.size() == expectedResult.size();
        Assert.assertTrue(result);
    }

}