package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        // TODO: Implement the logic here
        if (x == null || y == null) throw new IllegalArgumentException();
        if (x.size() == 0 || (x.size() == 0 && y.size() == 0)) return true;

        int tmpIndex = -1;
        List tmpList = y;
        for (Object obj : x) {
            tmpList = tmpList.subList(tmpIndex + 1, tmpList.size());
            tmpIndex = tmpList.indexOf(obj);
            if (tmpIndex == -1) return false;
        }
        return true;
    }
}
