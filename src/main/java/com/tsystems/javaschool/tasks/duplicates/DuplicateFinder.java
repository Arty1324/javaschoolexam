package com.tsystems.javaschool.tasks.duplicates;

import java.io.*;
import java.util.*;

public class DuplicateFinder {

    /**
     * Processes the specified file and puts into another sorted and unique
     * lines each followed by number of occurrences.
     *
     * @param sourceFile file to be processed
     * @param targetFile output file; append if file exist, create if not.
     * @return <code>false</code> if there were any errors, otherwise
     * <code>true</code>
     */
    public boolean process(File sourceFile, File targetFile) {
        // TODO: Implement the logic here
        if (sourceFile == null || targetFile == null) throw  new IllegalArgumentException();

        try (BufferedReader fileReader = new BufferedReader(new FileReader(sourceFile));
             FileWriter fileWriter = new FileWriter(targetFile, true)) {

            if (!sourceFile.exists()) throw new FileNotFoundException("Source file was not found!");
            if (!targetFile.exists()) targetFile.createNewFile();

            TreeMap<String, Integer> sortedLines = findDuplicates(readFromFile(fileReader));

            for (Map.Entry<String, Integer> pair : sortedLines.entrySet()) {
                fileWriter.write(pair.getKey() + "[" + pair.getValue() + "]" + System.lineSeparator());
            }
            fileWriter.flush();
        } catch (IOException e) {
            return false;
        }
        return true;
    }

    /**
     * Checks list of lines with the aim to find all duplicates.
     * Tree data structure sorts unique lines
     * and counts how many times this line was found.
     * @param inputLines list of read from file lines.
     * @return sorted map of unique lines and their repetition count.
     */
    TreeMap<String, Integer> findDuplicates(List<String> inputLines) {
        TreeMap<String, Integer> sortedLines = new TreeMap<>();
        for (String line : inputLines) {
            if (sortedLines.containsKey(line)) sortedLines.put(line, sortedLines.get(line) + 1);
            else sortedLines.put(line, 1);
        }
        return sortedLines;
    }

    /**
     * The task of this method is to read all of lines from input file,
     * put into list and then return this list.
     * @param fileReader file where needs to read lines.
     * @return list of read lines.
     * @throws IOException
     */
    private List<String> readFromFile(BufferedReader fileReader) throws IOException {
        List<String> linesFromFile = new LinkedList<>();
        String line;
        while ((line = fileReader.readLine()) != null) {
            linesFromFile.add(line);
        }
        return linesFromFile;
    }


}
