package com.tsystems.javaschool.tasks.calculator;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.LinkedList;
import java.util.Locale;

public class Calculator {

    private static final DecimalFormat formatter = Calculator.initFormatter();
    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        // TODO: Implement the logic here
        try {
            LinkedList<String> postfixExpression = makePostfix(statement.replaceAll(" ", ""));
            return execute(postfixExpression);
        } catch (Exception e) {
            return null;
        }
    }


    private static DecimalFormat initFormatter() {
        NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.ENGLISH);
        DecimalFormat decimalFormat = (DecimalFormat) numberFormat;
        decimalFormat.applyPattern("#.####");
        decimalFormat.setDecimalSeparatorAlwaysShown(false);
        return decimalFormat;
    }

    /**
     * This method gets arithmetic statement as argument and tries
     * to construct postfix form of this statement.
     * After constructing method try to execute received postfix statement.
     * @param statement arithmetic expression we need to calculate.
     * @return Postfix expression values list which will be executed.
     */
    private LinkedList<String> makePostfix (String statement) throws Exception{
        LinkedList<String> postfixExpression = new LinkedList<>();
        LinkedList<String> operationStack = new LinkedList<>();
        StringBuilder number = new StringBuilder();
        for (int i = 0; i < statement.length(); i++) {
            String symbol = String.valueOf(statement.charAt(i));
            String lastOperator;
            switch (symbol) {
                case "(":
                    tryAddNumberToPostfixExpr(number, postfixExpression);
                    operationStack.push(symbol);
                    break;
                case ")":
                    tryAddNumberToPostfixExpr(number, postfixExpression);
                    String operator;
                    while (!(operator = operationStack.pop()).equals("(")) {
                        postfixExpression.push(operator);
                    }
                    break;
                case "*":
                case "/":
                    tryAddNumberToPostfixExpr(number, postfixExpression);
                    lastOperator = operationStack.peek();
                    if (lastOperator == null || lastOperator.equals("+") || lastOperator.equals("-") || lastOperator.equals("(")) {
                        operationStack.push(symbol);
                    } else {
                        if (operationStack.size() != 0) postfixExpression.push(operationStack.pop());
                        operationStack.push(symbol);
                    }
                    break;
                case "+":
                case "-":
                    tryAddNumberToPostfixExpr(number, postfixExpression);
                    lastOperator = operationStack.peek();
                    if (lastOperator != null && !lastOperator.equals("(") && operationStack.size() != 0) {
                        postfixExpression.push(operationStack.pop());
                    }
                    operationStack.push(symbol);
                    break;
                default:
                    number.append(symbol);
                    if (i + 1 == statement.length() && !number.toString().equals("")) {
                        tryAddNumberToPostfixExpr(number, postfixExpression);
                    }
                    break;
            }
        }

        for (int j = 0; operationStack.size() > 0 && j <= operationStack.size(); j++) {
            postfixExpression.push(operationStack.pop());
        }
        return postfixExpression;
    }

    /**
     * Conversion of the string to a number and adding it to
     * postfix expression.
     * @param number some string which we need to add to the expression.
     * @param expression current postfix expression of the initial arithmetic statement.
     */
    private void tryAddNumberToPostfixExpr(StringBuilder number, LinkedList<String> expression) {
        if (!number.toString().equals("")) {
            expression.push(number.toString());
            number.delete(0, number.length());
        }
    }

    /**
     * Executing of the arithmetic statement.
     * @param expression postfix view of the initial arithmetic statement which will be calculated.
     * @return result of the calculation of our expression with formatting.
     */
    private String execute(LinkedList<String> expression) {
        LinkedList<String> result = new LinkedList<>();
        while(expression.size() > 0) {
            String tmp = expression.pollLast();
            double firstValue, secondValue;
            switch (tmp) {
                case "*":
                    result.push(String.valueOf(Double.parseDouble(result.pop())*Double.parseDouble(result.pop())));
                    break;
                case "/":
                    firstValue = Double.parseDouble(result.pop());
                    secondValue = Double.parseDouble(result.pop());
                    if (firstValue == 0) throw new NumberFormatException();
                    result.push(String.valueOf(secondValue/firstValue));
                    break;
                case "+":
                    result.push(String.valueOf(Double.parseDouble(result.pop())+Double.parseDouble(result.pop())));
                    break;
                case "-":
                    firstValue = Double.parseDouble(result.pop());
                    secondValue = Double.parseDouble(result.pop());
                    result.push(String.valueOf(secondValue - firstValue));
                    break;
                default:
                    result.push(tmp);
                    break;
            }
        }
        if (result.size() > 1) throw new IllegalStateException();
        return formatter.format(Double.parseDouble(result.poll()));
    }
}
